import { ExtraOptions, RouterModule, Routes } from "@angular/router";
import { NgModule } from "@angular/core";
import {
  NbAuthComponent,
  NbLoginComponent,
  NbLogoutComponent,
  NbRegisterComponent,
  NbRequestPasswordComponent,
  NbResetPasswordComponent
} from "@nebular/auth";
import { LoginComponent } from "./login/login.component";
import {
  AngularFireAuthGuard,
  canActivate,
  redirectUnauthorizedTo
} from "@angular/fire/auth-guard";
import { from } from "rxjs";

const redirectUnauthorizedToLanding = () => redirectUnauthorizedTo(["login"]);
const routes: Routes = [
  {
    path: "pages",
    loadChildren: () => import("./pages/pages.module").then(m => m.PagesModule),
    canActivate: [AngularFireAuthGuard],
    data: { authGuardPipe: redirectUnauthorizedToLanding }
  },
  {
    path: "auth",
    component: NbAuthComponent,
    canActivate: [AngularFireAuthGuard],
    data: { authGuardPipe: redirectUnauthorizedToLanding },
    children: [
      {
        path: "",
        component: NbLoginComponent
      },
      { path: "login", component: LoginComponent },
      {
        path: "register",
        component: NbRegisterComponent
      },
      {
        path: "logout",
        component: NbLogoutComponent
      },
      {
        path: "request-password",
        component: NbRequestPasswordComponent
      },
      {
        path: "reset-password",
        component: NbResetPasswordComponent
      }
    ]
  },
  { path: "login", component: LoginComponent },
  {
    path: "",
    redirectTo: "pages",
    pathMatch: "full",
    canActivate: [AngularFireAuthGuard],
    data: { authGuardPipe: redirectUnauthorizedToLanding }
  },
  {
    path: "**",
    redirectTo: "pages",
    canActivate: [AngularFireAuthGuard],
    data: { authGuardPipe: redirectUnauthorizedToLanding }
  }
];

const config: ExtraOptions = {
  useHash: false
};

@NgModule({
  imports: [RouterModule.forRoot(routes, config)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
