import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { LoginService } from '../app/login.service';


@Injectable({
  providedIn: 'root'
})
export class AdminAuthService implements CanActivate {

  constructor(private authService: LoginService) { }

  canActivate() {
    return this.authService.isAdmin();
  }
}
