import { NbMenuItem } from "@nebular/theme";
import { Component, OnInit } from "@angular/core";
import { LoginService } from "../login.service";
import { from } from "rxjs";
import { link } from "fs";

@Component({
  selector: "ngx-pages-menu",
  template: ``,
  styles: [``]
})
export class MenuComponent implements OnInit {
  static email: string;

  ngOnInit() {}
  constructor() {}
}

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: "Dashboard",
    icon: "home-outline",
    link: "/pages/dashboard",
    home: true
  },
  // {
  //   title: "IoT Dashboard",
  //   icon: "home-outline",
  //   link: "/pages/iot-dashboard"
  // },
  {
    title: "FEATURES",
    group: true
  },
  {
    title: "Checkout",
    icon: "shopping-cart-outline",
    link: "/pages/checkout"
  },
  {
    title: "Inventory Management",
    icon: "shopping-bag-outline",
    children: [
      {
        title: "Add Product",
        link: "/pages/inventory/add-product"
      },
      {
        title: "View Products",
        link: "/pages/inventory/view-product"
      },
      {
        title: "Barcode",
        link: "/pages/inventory/barcode"
      }
    ]
  },
  {
    title: "Staff Management",
    icon: "briefcase-outline",
    children: [
      {
        title: "Add memebers",
        link: "/pages/staffM/addnew"
      },
      {
        title: "View staff members",
        link: "/pages/staffM/viewmembers"
      }
    ]
  },
  {
    title: "Customer Management",
    icon: "people-outline",
    children: [
      {
        title: "Add customers",
        link: "/pages/customers/addnewcustomer"
      },
      {
        title: "View customers",
        link: "/pages/customers/viewcustomers"
      }
    ]
  },
  {
    hidden: false,

    title: "Genarate Reports",
    icon: "file-text-outline",
    children: [
      {
        title: "Daily Reports",
        link: "/pages/reportM/dailyReport"
      },
      // {
      //   title: "Monthly Reports",
      //   link: "/pages/reportM/monthlyReport"
      // }
    ]
  }
  // {
  //   title: "Layout",
  //   icon: "layout-outline",
  //   children: [
  //     {
  //       title: "Stepper",
  //       link: "/pages/layout/stepper"
  //     },
  //     {
  //       title: "List",
  //       link: "/pages/layout/list"
  //     },
  //     {
  //       title: "Infinite List",
  //       link: "/pages/layout/infinite-list"
  //     },
  //     {
  //       title: "Accordion",
  //       link: "/pages/layout/accordion"
  //     },
  //     {
  //       title: "Tabs",
  //       pathMatch: "prefix",
  //       link: "/pages/layout/tabs"
  //     }
  //   ]
  // }
  // // {
  //   title: "Forms",
  //   icon: "edit-2-outline",
  //   children: [
  //     {
  //       title: "Form Inputs",
  //       link: "/pages/forms/inputs"
  //     },
  //     {
  //       title: "Form Layouts",
  //       link: "/pages/forms/layouts"
  //     },
  //     {
  //       title: "Buttons",
  //       link: "/pages/forms/buttons"
  //     },
  //     {
  //       title: "Datepicker",
  //       link: "/pages/forms/datepicker"
  //     }
  //   ]
  // }
];
