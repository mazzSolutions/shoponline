import { Component, OnDestroy } from '@angular/core';
import { ProgressInfo, StatsProgressBarData } from '../../../@core/data/stats-progress-bar';
import { takeWhile } from 'rxjs/operators';
import { DashboardService } from '../dashboard.service';

@Component({
  selector: 'ngx-progress-section',
  styleUrls: ['./progress-section.component.scss'],
  templateUrl: './progress-section.component.html',
})
export class ECommerceProgressSectionComponent implements OnDestroy {

  private alive = true;

  progressInfoData;
  // progressInfoData: ProgressInfo[];
  totalProfit;
  totalItemCount;

  constructor(private statsProgressBarService: StatsProgressBarData,private dashboardService: DashboardService) {
    // this.statsProgressBarService.getProgressInfoData()
    //   .pipe(takeWhile(() => this.alive))
    //   .subscribe((data) => {
    //     this.progressInfoData = data;
    //     console.log(this.progressInfoData)
    //   });
      dashboardService.getWeek().subscribe(result=>{
        this.progressInfoData=result;
        this.totalProfit=result[0]["profit"];
        this.totalItemCount=result[0]["itemCount"]
      })
  }

  ngOnDestroy() {
    this.alive = true;
  }
}
