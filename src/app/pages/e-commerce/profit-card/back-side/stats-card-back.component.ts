import { Component, OnDestroy, OnInit } from "@angular/core";
import { StatsBarData } from "../../../../@core/data/stats-bar";
import { takeWhile } from "rxjs/operators";
import { DatePipe } from "@angular/common";
import { DashboardService } from "../../dashboard.service";
import { from } from "rxjs";

@Component({
  selector: "ngx-stats-card-back",
  styleUrls: ["./stats-card-back.component.scss"],
  templateUrl: "./stats-card-back.component.html"
})
export class StatsCardBackComponent implements  OnDestroy {
  private alive = true;
  myDate = new Date();
  finalDate: string;

  chartData: number[];

  constructor(
    private statsBarData: StatsBarData,
    private datePipe: DatePipe,
  ) {
    // this.statsBarData
    //   .getStatsBarData()
    //   .pipe(takeWhile(() => this.alive))
    //   .subscribe(data => {
    //     this.chartData = data;
    //     console.log(this.chartData);
    //   });

    this.finalDate = this.datePipe.transform(this.myDate, "yyyy.MM.dd"); //this is the user selected date
  }



  ngOnDestroy() {
    this.alive = false;
  }
}
