import { TestBed } from '@angular/core/testing';

import { TrafficCardService } from './traffic-card.service';

describe('TrafficCardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TrafficCardService = TestBed.get(TrafficCardService);
    expect(service).toBeTruthy();
  });
});
