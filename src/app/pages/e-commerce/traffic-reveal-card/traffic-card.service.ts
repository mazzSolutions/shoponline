import { Injectable } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";
import * as firebase from 'firebase/app';

@Injectable({
  providedIn: "root"
})
export class TrafficCardService {



  constructor(private afs: AngularFirestore) {

  }

  getWeek() {
    return this.afs.collectionGroup('orders').valueChanges();
  }
  // getWeek(start, end) {
  //   return this.afs.collection("orders", ref =>
  //     ref.where("date", ">", start).where("date", "<", end)
  //   );
  // }
}
