import { Component, OnDestroy } from '@angular/core';
import { TrafficList, TrafficListData } from '../../../@core/data/traffic-list';
import { TrafficBarData, TrafficBar } from '../../../@core/data/traffic-bar';
import { takeWhile } from 'rxjs/operators';

import { TrafficCardService } from './traffic-card.service';

@Component({
  selector: 'ngx-traffic-reveal-card',
  styleUrls: ['./traffic-reveal-card.component.scss'],
  templateUrl: './traffic-reveal-card.component.html',
})
export class TrafficRevealCardComponent implements OnDestroy {

  private alive = true;

  trafficBarData: TrafficBar;
  trafficListData;
  // trafficListData: TrafficList;
  revealed = false;
  period: string = 'week';

  constructor(private trafficListService: TrafficListData,
              private trafficBarService: TrafficBarData,private trafficCardService:TrafficCardService) {
    this.getTrafficFrontCardData(this.period);
    this.getTrafficBackCardData(this.period);
  }

  // toggleView() {
  //   this.revealed = !this.revealed;
  // }

  setPeriodAngGetData(value: string): void {
    this.period = value;

    this.getTrafficFrontCardData(value);
    this.getTrafficBackCardData(value);
  }

  getTrafficBackCardData(period: string) {
    this.trafficBarService.getTrafficBarData(period)
      .pipe(takeWhile(() => this.alive ))
      .subscribe(trafficBarData => {
        this.trafficBarData = trafficBarData;
      });
  }

  getTrafficFrontCardData(period: string) {
    // this.trafficListService.getTrafficListData(period)
    //   .pipe(takeWhile(() => this.alive))
    //   .subscribe(trafficListData => {
    //     this.trafficListData = trafficListData;
    //     console.log(this.trafficListData)
    //   });
      // this.trafficCardService.getWeek().subscribe(result=>{
      //   this.trafficListData=result;
      // });
      // this.trafficCardService.getWeek('2019-12-07', '2019-12-10').subscribe(result=>{
      //   console.log(result);
      // });
  }

  ngOnDestroy() {
    this.alive = false;
  }
}
