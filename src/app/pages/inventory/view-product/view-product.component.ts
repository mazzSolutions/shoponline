import { Component, OnInit } from "@angular/core";
import { ProductService } from "../product.service";

@Component({
  selector: "ngx-view-product",
  templateUrl: "./view-product.component.html",
  styleUrls: ["./view-product.component.scss"]
})
export class ViewProductComponent implements OnInit {
  source;

  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>'
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true
    },
    columns: {
      id: {
        title: "ID",
        width: "10px"
      },
      product_name: {
        title: "Item Name"
      },
      product_category: {
        title: "Item Type"
      },
      product_selling_price: {
        title: "Selling Price",
        width: "10px"
      },
      product_unit_price: {
        title: "Unit Price",
        width: "10px"
      },
      product_quantity: {
        title: "Quantity",
        width: "10px"
      }
    }
  };

  constructor(private productServeice: ProductService) {
    productServeice.getAll().subscribe(result => {
      this.source = result;
    });
  }

  ngOnInit() {}

  onDeleteConfirm(event): void {
    if (window.confirm("Are you sure you want to delete?")) {
      // event.confirm.resolve();
      // console.log(event.data.id);
      this.productServeice.delete(event.data.id);
    } else {
      event.confirm.reject();
    }
  }

  onSaveConfirm(event): void {
    if (window.confirm("Are you sure you want to edit this?")) {
      // event.confirm.resolve();
      // event.newData["name"] += " + added in code";
      // console.log(event.data);
      // console.log(event.newData);

      this.productServeice.edit(event.data.id, event.newData);
    } else {
      event.confirm.reject();
    }
  }
}
