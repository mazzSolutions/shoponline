import { NgModule } from "@angular/core";
import {
  InventoryRoutingModule,
  routedComponents
} from "./inventory-routing.module";
import {
  NbCardModule,
  NbSelectModule,
  NbButtonModule,
  NbInputModule,
  NbToastrModule,
  NbListModule
} from "@nebular/theme";
import { CommonModule } from "@angular/common";
import { Ng2SmartTableModule } from "ng2-smart-table";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { AngularFireModule } from "@angular/fire";
import { AngularFirestoreModule } from "@angular/fire/firestore";
import { environment } from "../../../environments/environment";
import { NbAlertModule } from "@nebular/theme";
import { from } from "rxjs";
import { BarcodeComponent } from "./barcode/barcode.component";
// import { NgxBarcodeModule } from "ngx-barcode";
import { Pdf417BarcodeModule } from "pdf417-barcode";
import { NgxBarcodeModule } from "ngx-barcode";
import { NgxBarcode6Module } from "ngx-barcode6";
import { Ng2SearchPipeModule } from "ng2-search-filter";

@NgModule({
  imports: [
    InventoryRoutingModule,
    NbCardModule,
    CommonModule,
    Ng2SmartTableModule,
    NbInputModule,
    NbButtonModule,
    NbSelectModule,
    ReactiveFormsModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig, "mymech"),
    AngularFirestoreModule,
    NbAlertModule,
    NbToastrModule.forRoot(),
    Pdf417BarcodeModule,
    NgxBarcodeModule,
    NgxBarcode6Module,
    NbListModule,
    Ng2SearchPipeModule
  ],
  declarations: [...routedComponents, BarcodeComponent],
  providers: []
})
export class InventoryModule {}
