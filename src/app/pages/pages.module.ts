import { NgModule } from "@angular/core";
import { NbMenuModule, NbDatepickerModule } from "@nebular/theme";

import { ThemeModule } from "../@theme/theme.module";
import { PagesComponent } from "./pages.component";
import { DashboardModule } from "./dashboard/dashboard.module";
import { ECommerceModule } from "./e-commerce/e-commerce.module";
import { PagesRoutingModule } from "./pages-routing.module";
import { MiscellaneousModule } from "./miscellaneous/miscellaneous.module";
import { InventoryComponent } from "./inventory/inventory.component";
import { CheckoutComponent } from "./checkout/checkout.component";
import { AddProductComponent } from "./inventory/add-product/add-product.component";
import { ViewProductComponent } from "./inventory/view-product/view-product.component";
import { CheckoutModule } from "./checkout/checkout.module";
import { AngularFireModule } from "@angular/fire";
import { AngularFirestoreModule } from "@angular/fire/firestore";
import { environment } from "../../environments/environment";
import { LoginComponent } from '../login/login.component';
import { MenuComponent } from '../pages/pages-menu';


@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    NbMenuModule,
    DashboardModule,
    ECommerceModule,
    MiscellaneousModule,
    CheckoutModule,
    AngularFireModule.initializeApp(environment.firebaseConfig, "mymech"),
    AngularFirestoreModule,
    NbDatepickerModule.forRoot()
  ],
  declarations: [PagesComponent, MenuComponent,]
})
export class PagesModule {}
