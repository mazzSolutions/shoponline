import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { CustomerComponent } from "./customer.component";
import { AddnewcustomerComponent } from "./addnewcustomer/addnewcustomer.component";
import { ViewcustomersComponent } from "./viewcustomers/viewcustomers.component";

const routes: Routes = [
  { path: "", component: CustomerComponent },
  { path: "addnewcustomer", component: AddnewcustomerComponent },
  { path: "viewcustomers", component: ViewcustomersComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerRoutingModule {}
