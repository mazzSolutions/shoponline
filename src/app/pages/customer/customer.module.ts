import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { CustomerRoutingModule } from "./customer-routing.module";
import { CustomerComponent } from "./customer.component";
import { AddnewcustomerComponent } from "./addnewcustomer/addnewcustomer.component";
import { ViewcustomersComponent } from "./viewcustomers/viewcustomers.component";
import { RouterModule } from "@angular/router";
import {
  NbCardModule,
  NbInputModule,
  NbSelectModule,
  NbButtonModule,
  NbToastrModule,
  NbAlertModule
} from "@nebular/theme";
import { Ng2SmartTableModule } from "ng2-smart-table";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { AngularFireModule } from "@angular/fire";
import { AngularFirestoreModule } from "@angular/fire/firestore";
import { environment } from "../../../environments/environment";

@NgModule({
  declarations: [
    CustomerComponent,
    AddnewcustomerComponent,
    ViewcustomersComponent
  ],
  imports: [
    CommonModule,
    CustomerRoutingModule,
    NbCardModule,
    NbInputModule,
    NbSelectModule,
    NbButtonModule,
    Ng2SmartTableModule,
    ReactiveFormsModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig, "mymech"),
    AngularFirestoreModule,
    NbToastrModule.forRoot(),
    NbAlertModule
  ]
})
export class CustomerModule {}
