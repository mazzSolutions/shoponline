import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { StaffRoutingModule } from "./staff-routing.module";
import { StaffComponent } from "./staff.component";
import { AddnewmemberComponent } from "./addnewmember/addnewmember.component";
import { ViewmembersComponent } from "./viewmembers/viewmembers.component";
import {
  NbCardModule,
  NbInputModule,
  NbSelectModule,
  NbButtonModule,
  NbToastrModule,
  NbAlertModule
} from "@nebular/theme";
import { from } from "rxjs";
import { Ng2SmartTableModule } from "ng2-smart-table";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { AngularFireModule } from "@angular/fire";
import { AngularFirestoreModule } from "@angular/fire/firestore";
import { environment } from "../../../environments/environment";

@NgModule({
  declarations: [StaffComponent, AddnewmemberComponent, ViewmembersComponent],
  imports: [
    CommonModule,
    StaffRoutingModule,
    NbCardModule,
    NbInputModule,
    NbSelectModule,
    NbButtonModule,
    Ng2SmartTableModule,
    ReactiveFormsModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig, "mymech"),
    AngularFirestoreModule,
    NbToastrModule.forRoot(),
    NbAlertModule
  ]
})
export class StaffModule {}
