import { Component, OnInit } from "@angular/core";
import { StaffMem, staffType, StaffService } from "../staff.service";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { NbToastrService } from "@nebular/theme";
import { from, Observable } from "rxjs";
import { type } from "os";

@Component({
  selector: "ngx-addnewmember",
  templateUrl: "./addnewmember.component.html",
  styleUrls: ["./addnewmember.component.scss"]
})
export class AddnewmemberComponent implements OnInit {
  staffTypes: Observable<staffType[]>;
  constructor(
    private staffService: StaffService,
    private toastrService: NbToastrService
  ) {}

  ngOnInit() {}

  addNewStaffForm = new FormGroup({
    first_name: new FormControl("", Validators.required),
    last_name: new FormControl("", Validators.required),
    email: new FormControl("", Validators.required),
    password: new FormControl("", Validators.required),
    contact_num: new FormControl("", Validators.required),
    type: new FormControl("", Validators.required)
  });

  get first_name() {
    return this.addNewStaffForm.get("first_name");
  }

  get last_name() {
    return this.addNewStaffForm.get("last_name");
  }

  get email() {
    return this.addNewStaffForm.get("email");
  }
  get password() {
    return this.addNewStaffForm.get("password");
  }

  get contact_num() {
    return this.addNewStaffForm.get("contact_num");
  }
  get type() {
    return this.addNewStaffForm.get("type");
  }

  get types() {
    return (this.staffTypes = this.staffService.StaffTypesget());
  }
  onSubmit(staff: StaffMem) {
    // TODO: Use EventEmitter with form value
    // console.warn(this.addProductForm.value);
    // console.log(this.addProductForm.value);
    this.staffService.SignUp(staff);
    console.log(staff);
    this.addNewStaffForm.reset();
    this.showToast("top-right", "success");
  }
  showToast(position, status) {
    this.toastrService.show("Staff Member Added Successfully", "Member Added", {
      position,
      status
    });
  }
}
