import { Component, OnInit } from "@angular/core";

@Component({
  selector: "ngx-staff",
  template: `
    <router-outlet></router-outlet>
  `,
  styleUrls: ["./staff.component.scss"]
})
export class StaffComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
