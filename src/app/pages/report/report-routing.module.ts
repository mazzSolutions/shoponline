import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { ReportComponent } from "./report.component";
import { DailyreportComponent } from "./dailyreport/dailyreport.component";
import { MonthlyreportComponent } from "./monthlyreport/monthlyreport.component";

const routes: Routes = [
  { path: "", component: ReportComponent },
  { path: "dailyReport", component: DailyreportComponent },
  {
    path: "monthlyReport",
    component: MonthlyreportComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportRoutingModule { }
