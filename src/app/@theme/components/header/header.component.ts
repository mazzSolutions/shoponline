import { Component, OnDestroy, OnInit } from "@angular/core";
import {
  NbMediaBreakpointsService,
  NbMenuService,
  NbSidebarService,
  NbThemeService,
  NbToastrService
} from "@nebular/theme";

import { UserData } from "../../../@core/data/users";
import { LayoutService } from "../../../@core/utils";
import { map, takeUntil, filter } from "rxjs/operators";
import { Subject, from } from "rxjs";
import { LoginService } from "../../../login.service";
import { Router } from "@angular/router";
import {
  ProductService,
  Product
} from "../../../../app/pages/inventory/product.service";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "ngx-header",
  styleUrls: ["./header.component.scss"],
  templateUrl: "./header.component.html"
})
export class HeaderComponent implements OnInit, OnDestroy {
  private destroy$: Subject<void> = new Subject<void>();
  userPictureOnly: boolean = false;
  user: any;
  userMail: string;
  pressed: boolean = false;
  lowitems: any[];
  lowItemsArray: { title: string }[] = new Array();
  lowItemsArray2: { title: string }[] = new Array();
  errorMsg: string = "";
  itemCount = 0;

  items = ["dulaj", "asss"];

  themes = [
    {
      value: "default",
      name: "Light"
    },
    {
      value: "dark",
      name: "Dark"
    },
    {
      value: "cosmic",
      name: "Cosmic"
    },
    {
      value: "corporate",
      name: "Corporate"
    }
  ];

  currentTheme = "default";

  userMenu = [{ title: "Profile" }, { title: "Log out" }];
  // itemspak = [{ title: "val" }, { title: "val2" }];

  constructor(
    private sidebarService: NbSidebarService,
    private menuService: NbMenuService,
    private themeService: NbThemeService,
    private userService: UserData,
    private layoutService: LayoutService,
    private breakpointService: NbMediaBreakpointsService,
    private lg: LoginService,
    private router: Router,
    private pService: ProductService,
    private toastrService: NbToastrService,
    private toastr: ToastrService
  ) {
    this.pService.getLowItems().subscribe(result => {
      this.lowitems = result;
      // console.log(this.lowitems);
    });

    // console.log(this.lowitems);
  }

  ngOnInit() {
    this.menuService
      .onItemClick()
      .pipe(
        filter(({ tag }) => tag === "my-menu"),
        map(({ item: { title } }) => title)
      )
      .subscribe(title => this.headerFun(title));
    this.currentTheme = this.themeService.currentTheme;

    this.userService
      .getUsers()
      .pipe(takeUntil(this.destroy$))
      .subscribe((users: any) => (this.user = users.nick));

    const { xl } = this.breakpointService.getBreakpointsMap();
    this.themeService
      .onMediaQueryChange()
      .pipe(
        map(([, currentBreakpoint]) => currentBreakpoint.width < xl),
        takeUntil(this.destroy$)
      )
      .subscribe(
        (isLessThanXl: boolean) => (this.userPictureOnly = isLessThanXl)
      );

    this.themeService
      .onThemeChange()
      .pipe(
        map(({ name }) => name),
        takeUntil(this.destroy$)
      )
      .subscribe(themeName => (this.currentTheme = themeName));

    this.userMail = this.lg.getMail();

    // this.lowItemsArray2 = this.fun();
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  changeTheme(themeName: string) {
    this.themeService.changeTheme(themeName);
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, "menu-sidebar");
    this.layoutService.changeLayoutSize();

    return false;
  }

  navigateHome() {
    this.menuService.navigateHome();
    return false;
  }
  headerFun(title: string) {
    if (title === "Log out") {
      this.lg.SignOut();
      console.log("sign out");
      this.router.navigate(["/login"]);
    }
  }
  fun() {
    // console.log("hiiii");
    // this.pressed = true;
    // this.pService.getLowItems().subscribe(result => {
    //   this.lowitems = result;
    //   console.log(this.lowitems);
    // });
    // console.log(this.lowitems);
    this.errorMsg = "";
    // this.lowItemsArray = null;
    this.lowitems.forEach(element => {
      // console.log(element);
      // console.log("Only " + element["product_quantity"] + " remaining");
      this.lowItemsArray.push({
        title:
          "Only " +
          element["product_quantity"] +
          " remaining " +
          " in " +
          element["product_name"]
      });

      // this.errorMsg = this.errorMsg + element["product_name"] + "\n\n";
      // console.log(this.lowItemsArray + "inside");
      this.lowItemsArray.forEach(element => {
        this.errorMsg = this.errorMsg + element["title"] + "<br/>";
      });
    });

    // console.log(this.errorMsg);
    if (this.errorMsg == "") {
    } else {
      // this.errorMsg = "fdfdfdfdfdf\n\nfdfssdf \n fdfdfs d \n";
      // this.showToast("top-right", "danger", this.errorMsg);
      this.showToast(this.errorMsg);
      this.errorMsg = "";
    }
  }

  // showToast(position, status, msg) {
  //   this.toastrService.show(msg, "Item Quantity is Low", {
  //     position,
  //     status
  //   });
  // }
  showToast(message) {
    this.toastr.warning(message, "Items Quantity is low", { enableHtml: true });
  }
}
