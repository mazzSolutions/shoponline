/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyAmvHrrd_5AIyXIs8Esthxza5fQn7wwBoI",
    authDomain: "warm-boutique-423b8.firebaseapp.com",
    databaseURL: "https://warm-boutique-423b8.firebaseio.com",
    projectId: "warm-boutique-423b8",
    storageBucket: "warm-boutique-423b8.appspot.com",
    messagingSenderId: "145509008722",
    appId: "1:145509008722:web:1904df432d3bd12fac268b",
    measurementId: "G-RDJT5D65LT"
  }
};
